import firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyBgLhDwoIXZaG-mRTZH6pHQmS9IRGE-gOQ",
    authDomain: "vuexcrud-b11c4.firebaseapp.com",
    databaseURL: "https://vuexcrud-b11c4.firebaseio.com",
    projectId: "vuexcrud-b11c4",
    storageBucket: "vuexcrud-b11c4.appspot.com",
    messagingSenderId: "64401212595",
    appId: "1:64401212595:web:590e3f827cc1e77b8a1cc6"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();

export { db };