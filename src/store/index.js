import Vue from 'vue';
import Vuex from 'vuex';
import { db } from '../firebase';

import router from '../router';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tareas: [],
    tarea: {name: '', id: ''}
  },
  mutations: {
    setTareas(state, payload){
      state.tareas = payload;
    },
    setTarea(state, payload){
      state.tarea = payload;
    }
  },
  actions: {
    getTareas({commit}){
      const tareas = []
      db.collection('tareas').get()
        .then(res => {
          res.forEach(res => {
            console.log(res.id);
            console.log(res.data());
            let tarea = res.data();
            tarea.id = res.id;
            tareas.push(tarea);
          });
          commit('setTareas', tareas)
        });
    },
    getTarea({commit}, idTarea){
      db.collection('tareas').doc(idTarea).get()
        .then(res => {
          console.log(res.id);
          console.log(res.data());
          let tarea = res.data();
          tarea.id = res.id;
          commit('setTarea', tarea);
        });
    },
    editTarea({commit}, tarea){
      db.collection('tareas').doc(tarea.id).update({
        name: tarea.name
      })
      .then(() => {
        console.log('Tarea editada');
        router.push('/');
      })
    },
    addTarea({commit}, nameTarea){
      db.collection('tareas').add({
        name: nameTarea
      })
      .then(doc => {
        console.log(doc.id);
        router.push('/');
      })
    }
  },
  modules: {
  }
});
